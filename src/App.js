import React, { useState, useEffect } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

class Lancement {
  constructor(id, name, heureLancement, nomConstructeur, codePays, nomMission, meteo) {
    this.id = id;
    this.name = name;
    this.heureLancement = heureLancement;
    this.nomConstructeur = nomConstructeur;
    this.codePays = codePays;
    this.nomMission = nomMission;
    this.meteo = meteo;
  }

}


const App = () => {
  const [listLancements, setListLancements] = useState(null);

  useEffect(() => {
    const urlLaunch='https://ll.thespacedevs.com/2.2.0/launch/upcoming';

    const fetchData = async () => {
      try {
        const response = await fetch(urlLaunch);
 
        //par default il renvoit 10 resultats
        if (!response.ok) {
          throw new Error(`Erreur HTTP : ${response.status}`);
        }

        const data = await response.json();

        const dataResultats=data.results;
  
        const lancements = dataResultats.map(resultat => new Lancement(
          resultat.id,
          resultat.name,
          resultat.rocket?.configuration?.manufacturer?.name,
          resultat.rocket?.configuration?.program?.start_date, 
          resultat.launch_service_provider?.country_code, 
          resultat.mission?.name,
          resultat.weather_concerns
          ));
          setListLancements(lancements);
        } catch (error) {
          console.error('Erreur lors de la requête GET:', error);
        }
      };
  
      fetchData();
    }, []);


    const columns = [{
      dataField: 'id',
      text: 'Id de Lancement'
    }, {
      dataField: 'name',
      text: 'Nom du lancement',
      filter: textFilter()
    }, {
      dataField: 'heureLancement',
      text: 'Heure de Lancement'
    },{
      dataField: 'nomConstructeur',
      text: 'Nom du constructeur'
    }, {
      dataField: 'codePays',
      text: 'Code du Pays'
    }, {
      dataField: 'nomMission',
      text: 'Nom de la mission'
    }, {
      dataField: 'meteo',
      text: 'Meteo'
    }
  ];

  return (
    <div>
      <h1>Les 10 derniers lancements</h1>
      {listLancements ? (
        <>
        <div className="custom-bootstrap-table">
          <BootstrapTable keyField='id' data={listLancements} columns={columns} filter={ filterFactory()}/>
          </div>
          </>
      ) : (
        <p>Veuillez patientez</p>
      )}
    </div>
  );
};

export default App;
/**
 * id: string
nom: name
heure lancement:rocket.configuration.manufacturer.name
constructeur: rocket.configuration.program.start_date
pays: launch_service_provider.country_code
weather_concerns
mission.name
 */